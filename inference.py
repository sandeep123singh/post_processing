'''
  Behavox assignement for post processing augemented data
'''

# load all the libraires
import pandas as pd
from transformers import XLNetTokenizer, XLNetForSequenceClassification
from transformers import BertTokenizer, BertForSequenceClassification
from sentence_transformers import SentenceTransformer, util
import json
import torch
import statistics
from torch.utils.data import TensorDataset,RandomSampler,SequentialSampler
from torch.utils.data import Dataset, DataLoader
import numpy as np
from scipy.special import softmax
import sys, getopt

# get and load sentence and grammmar models configs
sentence_pretrained_models_name = ['paraphrase-distilroberta-base-v2', 'stsb-roberta-base-v2', 'paraphrase-mpnet-base-v2', 'stsb-mpnet-base-v2']
model_config = {'xlnet': {'task':'xlnet',
                          'model_path':'xlnet_finetuned_cola_model',
                          'tokenizer_path':'xlnet_finetuned_cola_model',
                          'max_len':64,
                          'batch_size':1},
                'bert':{'task':'bert',
                        'model_path':'bert_finetuned_cola_model',
                        'tokenizer_path':'bert_finetuned_cola_model',
                        'max_len':64,
                        'batch_size':1}
}

# load all sentence similarity models
def load_pretrained_models():
  pararoberta_model = SentenceTransformer('paraphrase-distilroberta-base-v2')
  stsbroberta_model = SentenceTransformer('stsb-roberta-base-v2')
  parampnet_model = SentenceTransformer('paraphrase-mpnet-base-v2')
  stsbmpnet_model = SentenceTransformer('stsb-mpnet-base-v2')
  return [pararoberta_model, parampnet_model, stsbmpnet_model, stsbmpnet_model]

# get the cosine similarity 
def get_cosine_score(model, sentence1, sentence2):
  sent1_emb = model.encode(sentence1, convert_to_tensor=True)
  sent2_emb = model.encode(sentence2, convert_to_tensor=True)
  cosine_scores = util.pytorch_cos_sim(sent1_emb, sent2_emb)
  return cosine_scores

# ensemble all the models similarity score to choose similar/disimilar/neutral score
def ensemble_model_scores(sentence1, sentence2, models):
  similarity_labels = []
  agregate_result_label = "disimilar"

  for model in models:
    similarity_score = get_cosine_score(model, sentence1, sentence2)
    similarity_labels.append(round(similarity_score.item(),2))

  avg_similar_score = statistics.mean(similarity_labels)

  if avg_similar_score >= 0.4 and avg_similar_score <= 0.8:
    agregate_result_label = 'neutral'
  elif avg_similar_score > 0.8:
    agregate_result_label = 'similar'
  else:
    agregate_result_label = 'disimilar'

  return (agregate_result_label, avg_similar_score, similarity_labels)

# create the check / meta data structure for similarity check
def create_similarity_checks_structure(semantic_pred, avg_similar_score, simialry_models_score, final_pred, sentence_pretrained_models_name):
  similarity_structure = {}
  similarity_structure['similarity_check'] = final_pred
  similarity_structure['similarity_prediction'] = semantic_pred
  similarity_structure['average_similarity_score'] = avg_similar_score
  similarity_structure[sentence_pretrained_models_name[0]+'_similarity_score'] = simialry_models_score[0]
  similarity_structure[sentence_pretrained_models_name[1]+'_similarity_score'] = simialry_models_score[1]
  similarity_structure[sentence_pretrained_models_name[2]+'_similarity_score'] = simialry_models_score[2]
  similarity_structure[sentence_pretrained_models_name[3]+'_similarity_score'] = simialry_models_score[3]
  
  if avg_similar_score >= 0.4 and avg_similar_score <= 0.8:
    similarity_structure['similarity_model_threshold'] = "0.4 - 0.8"
  elif avg_similar_score > 0.8:
    similarity_structure['similarity_model_threshold'] = "> 0.8"
  else:
    similarity_structure['similarity_model_threshold'] = "< 0.4"
  return similarity_structure

# create the check / meta data structure for gammar check
def create_grammar_checks_structure(grammar_pred, grammar_model_pred, grammar_model_prob, final_pred):
  grammar_check_structure = {}
  grammar_check_structure['grammar_check'] = grammar_pred
  # grammar_check_structure['grammar_prediction'] = grammar_pred
  grammar_check_structure['grammar_xlnet_model_prediction'] = grammar_model_pred[0]
  grammar_check_structure['grammar_bert_model_prediction'] = grammar_model_pred[1]
  grammar_check_structure['grammar_xlnet_model_prediction_prob'] = str([round(scr,3) for scr in grammar_model_prob[0]])
  grammar_check_structure['grammar_bert_model_prediction_prob'] = str([round(scr,3) for scr in grammar_model_prob[1]])
  grammar_check_structure['grammar_model_threshold'] = "> = 0.5"
  grammar_check_structure['augmentation_check'] = final_pred
  return grammar_check_structure


def check_gpu_availabilty():
  if torch.cuda.is_available():        
      device = torch.device("cuda")
      print('There are %d GPU(s) available.' % torch.cuda.device_count())
      print('We will use the GPU:', torch.cuda.get_device_name(0))
  else:
      print('No GPU available, using the CPU instead.')
      device = torch.device("cpu")
  return device

# load the tokenizers
def get_tokenizer(task,pre_trained_model_path):
  print('Loading  tokenizer...')
  tokenizer = task.from_pretrained(pre_trained_model_path)
  return tokenizer

# load the models
def load_model(task,pre_trained_model_path, classes, device):
  model = task.from_pretrained(pre_trained_model_path, num_labels = len(classes), output_attentions = False, output_hidden_states = False)
  model = model.to(device)
  return model

# create a torch dataloader
def create_dataloader(dataset, batch_size, Sampler):
  dataloader = DataLoader(
            dataset,
            sampler = Sampler(dataset),
            batch_size = batch_size
        )
  return dataloader

# encode the text for attention and input mask
def encode_text(sent, tokenizer, max_length):
  enc = tokenizer.encode_plus(sent, add_special_tokens=True, 
                              max_length=max_length, 
                              return_tensors='pt', 
                              return_token_type_ids=False, 
                              return_attention_mask=True, 
                              pad_to_max_length=True)
  return enc

# inference model
def inference(model, prediction_dataloader, device):
  model.eval() 
  predictions , top_pred = [], []
  for batch in prediction_dataloader:
    batch = tuple(t.to(device) for t in batch)
    b_input_ids, b_input_mask = batch
    with torch.no_grad():
        result = model(b_input_ids, 
                      token_type_ids=None, 
                      attention_mask=b_input_mask,
                      return_dict=True)
    logits = result.logits
    logits = logits.detach().cpu().numpy()
    top_pred.extend(np.argmax(softmax(logits, axis=1),axis=1))
    predictions.extend(softmax(logits, axis=1))
  return top_pred, predictions

# get all the models for grammar check
def get_all_models(config):
  models = []
  tokenizers = []
  device = check_gpu_availabilty()
  classes = ['yes','no']

  if 'xlnet' in config:
    tokenizers.append(get_tokenizer(XLNetTokenizer, config['xlnet']['tokenizer_path']))
    models.append(load_model(XLNetForSequenceClassification, config['xlnet']['model_path'],classes, device))

  if 'bert' in config:
    tokenizers.append(get_tokenizer(BertTokenizer, config['bert']['tokenizer_path']))
    models.append(load_model(BertForSequenceClassification, config['bert']['model_path'], classes, device))
  return models, tokenizers, device

# ensemble bert and xlnet model for grammar check model
def get_grammar_pred(models, tokenizers, sent, config, device):
  grammar_class_pred = []
  grammar_class_pred_prob = []
  ensemble_grammar_pred = "failed"
  id2label = {0:'no',1:'yes'}

  for i in range(0, len(models)):
    pred_model = models[i]
    pred_tokenizer = tokenizers[i]
    max_len = config['xlnet']['max_len']
    batch_size = config['xlnet']['batch_size']
    encoded_dict = encode_text(sent, pred_tokenizer, max_len)
    pred_dataloader = create_dataloader(TensorDataset(encoded_dict['input_ids'], encoded_dict['attention_mask']), batch_size, SequentialSampler)
    top_pred, predictions = inference(pred_model, pred_dataloader, device)
    grammar_class_pred.append(id2label[top_pred[0]])
    grammar_class_pred_prob.append(list(predictions[0]))

  if 'yes' in grammar_class_pred:
    ensemble_model_scores = 'passed'
  else:
    ensemble_model_scores = 'failed'
  return (ensemble_model_scores, grammar_class_pred, grammar_class_pred_prob)

# main function to drive and store the results
def main(argv):
  inputfile = ''
  outputfile = ''
  try:
     opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
  except getopt.GetoptError:
     print('inference.py -i <inputfile> -o <outputfile>')
     sys.exit(2)
  for opt, arg in opts:
     if opt == '-h':
        print('inference.py -i <inputfile> -o <outputfile>')
        sys.exit()
     elif opt in ("-i", "--ifile"):
        inputfile = arg
     elif opt in ("-o", "--ofile"):
        outputfile = arg

  print('Input file is "', inputfile)
  print('Output file is "', outputfile)

  sentence_pretrained_models = load_pretrained_models()
  grammr_models, grammar_tokenizers, device = get_all_models(model_config)
  input_file_path = inputfile

  passed_sents = []
  failed_sents = []
  post_processed_augemented_results = {}

  with open(input_file_path) as fp:
    data = json.load(fp)
    for key, value in data.items():
      reference_sent = key
      for augmented_sent in value:
        final_pred = None
        grammar_pred = None
        similarity_pred = None
        similarity_check_struct = {}
        grammar_check_struct = {}

        # similarity check
        augmented_sent_results = ensemble_model_scores(reference_sent, augmented_sent, sentence_pretrained_models)
        aug_similarity_pred = augmented_sent_results[0]
        avg_similarity_score = augmented_sent_results[1]
        model_similarity_score = augmented_sent_results[2]

        if aug_similarity_pred == ' neutral':
          similarity_pred = 'passed'
        else:
          similarity_pred = 'failed'

        # grammar check
        grammar_pred_results = get_grammar_pred(grammr_models, grammar_tokenizers,augmented_sent,model_config, device)
        grammar_pred = grammar_pred_results[0]
        grammar_model_pred = grammar_pred_results[1]
        grammar_model_prob = grammar_pred_results[2]


        if aug_similarity_pred == 'neutral' and grammar_pred=='passed':
          final_pred = "passed"
        else:
          final_pred = "failed"
        
        if final_pred:
          similarity_check_struct = create_similarity_checks_structure(aug_similarity_pred, avg_similarity_score,model_similarity_score, similarity_pred, sentence_pretrained_models_name)
          grammar_check_struct = create_grammar_checks_structure(grammar_pred, grammar_model_pred, grammar_model_prob, final_pred)
          similarity_check_struct.update(grammar_check_struct)
        
        if final_pred and final_pred=='passed':
          passed_sents.append({'augmentation':augmented_sent,"checks":similarity_check_struct})
        else:
          failed_sents.append({'augmentation':augmented_sent,"checks":similarity_check_struct})
  post_processed_augemented_results['passed'] = passed_sents
  post_processed_augemented_results['failed'] = failed_sents

  with open(outputfile, "w") as outfile: 
    json.dump(post_processed_augemented_results, outfile)


if __name__ == "__main__":
   main(sys.argv[1:])